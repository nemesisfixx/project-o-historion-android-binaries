# The Historion (Binaries Repo) #

GRAB the APK: http://tiny.cc/historion-apk
![VIA QRCODE](https://tiny.cc/historion-apk/qr/image/H/300)

This repo contains the binaries (*.apk) for the Project-O's Historion implementation for the Android platform. Typically, once you get hold of any version of the Historion, you never need to manually download and install subsequent versions anymore, as the Historion is designed to automatically check for and prompt you to install any new versions of itself. Also, within the Historion, you can both query for, and install any published new versions if for some reason automatic updating isn't working.

**NOTE**: this is the binaries repo, thus, the actual code of the project resides elsewhere.

------
Contact NuChwezi Tech support if you find any difficulties installing or updating your Historion.